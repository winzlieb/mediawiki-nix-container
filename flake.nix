{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";

  outputs = { self, nixpkgs }: {

    devShell.x86_64-linux = import ./shell.nix {
      pkgs = import nixpkgs { system =  "x86_64-linux"; };
    };

    nixosConfigurations.container =  nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules =
        [ ({ config, pkgs, lib, ... }:
        let
          mediaWikiOld = pkgs.mediawiki.overrideAttrs ({pname, ...}: rec {
            version = "1.38.1";
            src = with lib; pkgs.fetchurl {
              url = "https://releases.wikimedia.org/mediawiki/${versions.majorMinor version}/${pname}-${version}.tar.gz";
              sha256 = "sha256-EXNlUloN7xsgnKUIV9ZXNrYlRbh3p1NIpXqF0SZDezE=";
            };
          });
        in
        {
            #imports = [ ./module/mediawiki.nix ];
            boot.isContainer = true;

            # Let 'nixos-version --json' know about the Git revision
            # of this flake.
            system.configurationRevision = nixpkgs.lib.mkIf (self ? rev) self.rev;

            # Network configuration.
            networking.useDHCP = false;
            networking.firewall.allowedTCPPorts = [ 80 5432 ];

            services.postgresql =
              let
                cfg = config.services.mediawiki;
              in {
                enable = true;
                enableTCPIP = true;
                package = pkgs.postgresql_11;
                ensureDatabases = [ cfg.database.name ];

                ensureUsers = [{ 
                  name = cfg.database.user;
                  ensurePermissions = { "DATABASE ${cfg.database.name}" = "ALL PRIVILEGES"; };
                }              
              ];
              authentication = lib.mkForce ''
                # TYPE  DATABASE        USER            ADDRESS                 METHOD
                  local   all             all                                     trust
                  host    all             all             127.0.0.1/32            trust
                  host    all             all             10.233.2.1/32            trust
                  host    all             all             169.254.155.231/32      trust
                  host    all             all             ::1/128                 trust 
              '';};

              system.stateVersion = "22.05";

              services.mediawiki = let
                cfg = config.services.mediawiki;
              in {
                enable = true;
                package = mediaWikiOld;
                virtualHost = {
                  hostName = "mediawiki";
                  adminAddr = "root@example.com";
                };
              #skins = {
              #      Vector = "${mediaWikiOld}/share/mediawiki/skins/Vector";
              #      Hector = "${mediaWikiOld}/share/mediawiki/skins/Hector";
              #};
              name = "C3D2";

              extraConfig = ''
                $wgShowExceptionDetails = true;
                $wgDBserver = "${cfg.database.socket}";
                $wgDBmwschema       = "mediawiki";

                $wgLogo =  "https://www.c3d2.de/images/ck.png";
                $wgEmergencyContact = "wiki@c3d2.de";
                $wgPasswordSender   = "wiki@c3d2.de";
                $wgLanguageCode = "de";

                $wgGroupPermissions['*']['edit'] = false;
                $wgGroupPermissions['user']['edit'] = true;
                $wgGroupPermissions['sysop']['interwiki'] = true;
                $wgGroupPermissions['sysop']['userrights'] = true;

                define("NS_INTERN", 100);
                define("NS_INTERN_TALK", 101);

                $wgExtraNamespaces[NS_INTERN] = "Intern";
                $wgExtraNamespaces[NS_INTERN_TALK] = "Intern_Diskussion";

                $wgGroupPermissions['intern']['move']             = true;
                $wgGroupPermissions['intern']['move-subpages']    = true;
                $wgGroupPermissions['intern']['move-rootuserpages'] = true; // can move root userpages
                $wgGroupPermissions['intern']['read']             = true;
                $wgGroupPermissions['intern']['edit']             = true;
                $wgGroupPermissions['intern']['createpage']       = true;
                $wgGroupPermissions['intern']['createtalk']       = true;
                $wgGroupPermissions['intern']['writeapi']         = true;
                $wgGroupPermissions['intern']['upload']           = true;
                $wgGroupPermissions['intern']['reupload']         = true;
                $wgGroupPermissions['intern']['reupload-shared']  = true;
                $wgGroupPermissions['intern']['minoredit']        = true;
                $wgGroupPermissions['intern']['purge']            = true; // can use ?action=purge without clicking "ok"
                $wgGroupPermissions['intern']['sendemail']        = true;

                $wgNamespacePermissionLockdown[NS_INTERN]['*'] = array('intern');
                $wgNamespacePermissionLockdown[NS_INTERN_TALK]['*'] = array('intern');

#NS i4r
                define("NS_I4R", 102);
                define("NS_I4R_TALK", 103);
                $wgExtraNamespaces[NS_I4R] = "IT4Refugees";
                $wgExtraNamespaces[NS_I4R_TALK] = "IT4Refugees_Diskussion";
                $wgGroupPermissions['i4r']['move']             = true;
                $wgGroupPermissions['i4r']['move-subpages']    = true;
                $wgGroupPermissions['i4r']['move-rootuserpages'] = true; // can move root userpages
                $wgGroupPermissions['i4r']['read']             = true;
                $wgGroupPermissions['i4r']['edit']             = true;
                $wgGroupPermissions['i4r']['createpage']       = true;
                $wgGroupPermissions['i4r']['createtalk']       = true;
                $wgGroupPermissions['i4r']['writeapi']         = true;
                $wgGroupPermissions['i4r']['upload']           = true;
                $wgGroupPermissions['i4r']['reupload']         = true;
                $wgGroupPermissions['i4r']['reupload-shared']  = true;
                $wgGroupPermissions['i4r']['minoredit']        = true;
                $wgGroupPermissions['i4r']['purge']            = true; // can use ?action=purge without clicking "ok"
                $wgGroupPermissions['i4r']['sendemail']        = true;
                $wgNamespacePermissionLockdown[NS_I4R]['*'] = array('i4r');
                $wgNamespacePermissionLockdown[NS_I4R_TALK]['*'] = array('i4r');

                $wgGroupPermissions['sysop']['deletelogentry'] = true;
                $wgGroupPermissions['sysop']['deleterevision'] = true;

                $wgEnableAPI = true;
                $wgAllowUserCss = true;
                $wgUseAjax = true;
                $wgEnableMWSuggest = true;

                $wgScribuntoDefaultEngine = 'luastandalone';
              '';
              extensions = {
                Interwiki = pkgs.fetchzip {
                  url = "https://gitea.c3d2.de/C3D2/mediawiki-common-extentions/raw/commit/ea2cb04de7194300284b330104ae0bf33ef05988/Interwiki-REL1_38-223bbf8.tar.gz";
                  sha256 = "sha256-A4tQuISJNzzXPXJXv9N1jMat1VuZ7khYzk2jxoUqzIk=";
                };
                Cite = pkgs.fetchzip {
                  url = "https://gitea.c3d2.de/C3D2/mediawiki-common-extentions/raw/commit/ea2cb04de7194300284b330104ae0bf33ef05988/Cite-REL1_38-7fdd57d.tar.gz";
                  sha256 = "sha256-/s9byrAVjky0EeiokUEchG3ICw+Q2T6HLjbzHnl3uVE=";
                };
                ConfirmEdit = pkgs.fetchzip {
                  url = "https://gitea.c3d2.de/C3D2/mediawiki-common-extentions/raw/commit/ea2cb04de7194300284b330104ae0bf33ef05988/ConfirmEdit-REL1_38-9ae04a5.tar.gz";
                  sha256 = "sha256-iiRT98uUmy1gvKzl/5ijheAAjUK3BLewt8IG8qdCHsA=";
                };
                CiteThisPage = pkgs.fetchzip {
                  url = "https://gitea.c3d2.de/C3D2/mediawiki-common-extentions/raw/commit/70e6bf9c24f552cd1f459089e7e76685e678871d/CiteThisPage-REL1_38-157e3bc.tar.gz";
                  sha256 = "sha256-q2z4y4Afcq98/Dh6kQqZxeUg9fYFv9ntR+UyelHLDKc=";
                };
                ParserFunctions = pkgs.fetchzip {
                  url = "https://gitea.c3d2.de/C3D2/mediawiki-common-extentions/raw/commit/70e6bf9c24f552cd1f459089e7e76685e678871d/ParserFunctions-REL1_38-c2ccf36.tar.gz";
                  sha256 = "sha256-z3Gwl/xzFBUUAm9u6ixgfJgrO5oTopXGuXEpaewUG1Y=";
                };
                SyntaxHightlight = pkgs.fetchzip {
                  url = "https://gitea.c3d2.de/C3D2/mediawiki-common-extentions/raw/commit/70e6bf9c24f552cd1f459089e7e76685e678871d/SyntaxHighlight_GeSHi-REL1_38-150f839.tar.gz";
                  sha256 = "sha256-miXbsf2TdalEkUGiVrh55q3NuEtmnQQbb0f1XBmSilw=";
                };
                intersection = pkgs.fetchzip {
                  url = "https://gitea.c3d2.de/C3D2/mediawiki-common-extentions/raw/commit/70e6bf9c24f552cd1f459089e7e76685e678871d/intersection-REL1_38-8525097.tar.gz";
                  sha256 = "sha256-shgA0XLG6pgikqldOfda40hV9zC1eBp+NalGhevFq2Q=";
                };
                #DynamicPageList = pkgs.fetchzip {
                #  url = "https://extdist.wmflabs.org/dist/extensions/DynamicPageList-REL1_38-3b7a26d.tar.gz";
                #  sha256 = "sha256-WjVLks0Q9hSN2poqbKzTJhvOXog7UHJqjY2WJ4Uc64o=";
                #};
                Scribunto = pkgs.fetchzip {
                  url = "https://gitea.c3d2.de/C3D2/mediawiki-common-extentions/raw/commit/70e6bf9c24f552cd1f459089e7e76685e678871d/Scribunto-REL1_38-bd2f615.tar.gz";
                  sha256 = "sha256-e70P8/0CsWWftyh2LhFw/Fv3E34Bl8HIZxVszuUl8Pk=";
                };
                Lockdown = pkgs.fetchzip {
                  url = "https://gitea.c3d2.de/C3D2/mediawiki-common-extentions/raw/commit/70e6bf9c24f552cd1f459089e7e76685e678871d/Lockdown-REL1_38-1915db4.tar.gz";
                  sha256 = "sha256-YCYsjh/3g2P8oT6IomP3UWjOoggH7jYjiiix7poOYnA=";
                };
              };
              passwordFile = pkgs.writeText "password" "topSecretF0rAll!!!!";
              database = {
                type = "postgres";
                socket = "/run/postgresql";
                user = "mediawiki";
                name = "mediawiki";
              };
              uploadsDir = "/var/lib/mediawiki/uploads";
            };
          })
        ];
      };

    };
  }
