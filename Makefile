nixos-rebuild:
	sudo nixos-container update mediawiki --flake .#container

nixos-create: 
	sudo nixos-container create mediawiki --system-path `realpath container` --flake .#container

nixos-update: nixos-stop nixos-rebuild nixos-start

nixos-stop:
	sudo nixos-container stop mediawiki

nixos-start:
	sudo nixos-container start mediawiki

nixos-login: nixos-start
	sudo nixos-container root-login mediawiki

nixos-destroy:
	sudo nixos-container destroy mediawiki
